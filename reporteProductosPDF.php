<?php
require "fpdf/fpdf.php";
require_once 'logica/Producto.php';

$pdf = new FPDF("P", "mm", "Letter");
$producto = new Producto();
$productos= $producto-> consultarTodos();
$pdf -> SetFont("Courier", "B", 20);
$pdf -> AddPage();
$pdf ->SetXY(0, 0);
$pdf -> Cell(216, 20, "Tienda Virtual", 0, 2, "C");
$pdf -> Cell(216, 15, "Reporte Productos", 0, 2, "C");

$header = array(
    "#", "Nombre","Precio"
);
$i=0;
$j=0;

//$imagen=$productos->getImagen();
$w = array(10, 70, 50);
// Cabeceras
for($i=0;$i<count($header);$i++)
    $pdf->Cell($w[$i],7,$header[$i],1);
    $pdf->Ln();
    
    // Datos
   
   foreach($productos as $p)
    {
        
        $pdf->Cell(70,16,$p->getNombre(),1);
       
        $pdf->Cell(50,16,$p->getPrecio(),1);
        //$pdf->Cell( 60, 40, $pdf->Image($p->getImagen(), $pdf->GetX(), $pdf->GetY(), 20,), 1, 0, 'C', false );
       // $pdf->Cell($w[4],6,($row[3]),'LR');
        $j++;
        $pdf->Ln();
        
       
    }
  
    // L�nea de cierre
    $pdf->Cell(array_sum($w),0,'','T');
    $pdf -> Output();
    ?>
   
  


