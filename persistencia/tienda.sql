create database tienda;
use tienda;

create table producto(
id int auto_increment not null,
nombre varchar(45),
precio double,
primary key (id));

create table tienda(
id int auto_increment not null,
nombre varchar(45),
direccion varchar(45),
primary key (id)
);

create table producto_tienda(
id_producto int not null,
id_tienda int not null,
unidades_p int,
foreign key(id_producto) references producto(id),
foreign key(id_tienda) references tienda(id),
primary key(id_producto,id_tienda)

);