<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/TiendaDAO.php";
class Tienda{
    private $idTienda;
    private $nombre;
 
    private $direccion;

    
    private $tiendaDAO;
    
    public function getIdTienda(){
        return $this -> idTienda;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    

    public function getDireccion(){
        return $this -> direccion;
    }
        

    
    public function Tienda($idTienda = "", $nombre = "", $direccion = ""){
        $this -> idTienda = $idTienda;
        $this -> nombre = $nombre;
        
        $this -> direccion = $direccion;
        
        $this -> conexion = new Conexion();
        $this -> tiendaDAO = new TiendaDAO($this -> idTienda, $this -> nombre, $this -> direccion);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tiendaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> cantidad = $resultado[1];
        $this -> direccion = $resultado[2];
      
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> tiendaDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tiendaDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Tienda($resultado[0], $resultado[1], $resultado[2]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }
    

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tiendaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
   
       
        
      
    
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tiendaDAO -> editar());
        $this -> conexion -> cerrar();
    }
  
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> tiendaDAO -> consultarFiltro($filtro));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Tienda($resultado[0], $resultado[1], $resultado[2]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    
}
