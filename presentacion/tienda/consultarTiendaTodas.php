<?php
require_once 'logica/Tienda.php';
$tienda = new Tienda();
$tiendas = $tienda -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Tienda</h4>
				</div>
				<div class="text-right"><?php echo count($tiendas) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
						
							<th>Precio</th>
							
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($tiendas as $t){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $t -> getNombre() . "</td>";
						   
						    echo "<td>" . $t-> getDireccion() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>